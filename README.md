# Kittybox Micropub endpoint

[![pipeline status](https://gitlab.com/vikanezrimaya/kittybox/badges/master/pipeline.svg)](https://gitlab.com/vikanezrimaya/kittybox/commits/master)
[![coverage report](https://gitlab.com/vikanezrimaya/kittybox/badges/master/coverage.svg)](https://gitlab.com/vikanezrimaya/kittybox/commits/master)

This is a Micropub endpoint for Kittybox, an IndieWeb-enabled overengineered
blog solution.

## Supported features
 - Stores native microformats
   - Accepts `h-entry`, `h-card` and `h-event`
   - <3 JSON but accepts form-encoded too
 - Configurable backend storage
   - Redis
   - MongoDB (planned)
   - Flat-file (planned)
 - Container-friendly
 - Private posts support (not shown on feeds without authentication; needs
   KittyboxQL support)
 - Own filter syntax for filtering feeds called KittyQL
   - There are too many solutions in this field, and this is another one
   - Described in [this](https://fireburn.ru/posts/1574643184) blog post by me
   - Once there is an accepted standard solution, it will replace KittyQL
 - Prometheus metrics suppor
 - Syndication and Webmentions (syndication endpoints are currently hardcoded)

## Planned features
 - Configuring syndication endpoints (#7)
 - Pinging WebSub (#3)
 - Markdown (#4)
 - Auto hashtagging (#5)
 - Nickname cache from stored h-cards (#6)
