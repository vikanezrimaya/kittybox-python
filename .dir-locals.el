((nil . ())
 (python-mode . ((lsp-pyls-server-command . (let ((shellfile . (concat (locate-dominating-file buffer-file-name ".dir-locals.el") "shell.nix")) (command (concat "which nix-shell && { [[ -f " shellfile " ]] && nix-shell " shellfile " --run pyls || nix-shell -p python3Packages.pyls --run pyls; } || pyls")))
                                              '("bash" "-c" command)
                                              )
                                          )))
