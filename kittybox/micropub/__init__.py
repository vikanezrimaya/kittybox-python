"""Micropub microservice using Starlette."""
import sys
import datetime
import time
import copy
import logging
from typing import List, Optional, Dict, Union, Tuple, Any, cast
import urllib.parse
import aiohttp
import dateutil.parser
from memoize.wrapper import memoize
from starlette.applications import Starlette
from starlette.requests import Request, HTTPConnection
from starlette.responses import Response, UJSONResponse
from starlette.routing import Route, Mount
from starlette.exceptions import HTTPException
from starlette.datastructures import FormData
from starlette.authentication import (
    AuthenticationBackend, AuthenticationError, SimpleUser, BaseUser,
    AuthCredentials, requires
)
from starlette.middleware import Middleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.cors import CORSMiddleware
from starlette.background import BackgroundTask
from starlette.config import Config
from starlette_prometheus import PrometheusMiddleware, metrics
from kittybox.micropub.mf2object import MF2Object, convert_to_mf2object
from kittybox.micropub.backends import RedisBackend  # , MongoBackend
from kittybox.micropub.mq import RedisMessageQueue
from kittybox.micropub.newbase60 import numtosxg
from kittybox.micropub.filter_parser import parse_filter_query
from kittybox.micropub.webmention import WebmentionSender

logging.basicConfig(level=logging.INFO)
logging.getLogger(__name__).setLevel(logging.DEBUG)


class IndieAuthBackend(AuthenticationBackend):
    """IndieAuth token authentication backend for Starlette's
    AuthenticationMiddleware."""

    def __init__(self) -> None:
        # This is actually of type `aiohttp.ClientSession`
        self.http_client: Any = None
        # This is actually a str
        self.token_endpoint: Any = None

    @memoize()
    async def check_token(self, auth: str) -> dict:
        """Check the token using the token endpoint.

        Note: this should be called from authenticate() at least once.
        """
        try:
            # Mypy gives union-attr on this line.
            # It doesn't realize that authenticate() guarantees that
            # this will never be None when this function is called.
            async with self.http_client.get(
                    self.token_endpoint,
                    headers={"Authorization": auth}) as response:
                return await response.json()
        # I have no way of testing if this throws an exception.
        # Or do I?
        except aiohttp.ClientResponseError:  # pragma: no cover
            raise AuthenticationError("invalid_token")

    async def authenticate(self, conn: HTTPConnection) -> Tuple[
            AuthCredentials, BaseUser]:
        if self.http_client is None:
            self.http_client = conn.app.state.http_client
        if self.token_endpoint is None:
            self.token_endpoint = conn.app.state.token_endpoint
        auth = None
        token = None
        if "Authorization" in conn.headers:
            auth = conn.headers["Authorization"]
        # If the token isn't found we're forced to read the body.
        elif conn.headers.get("content-type") in (
                "multipart/form-data",
                "application/x-www-form-urlencoded"):
            # We can't read the body here. Starlette
            # doesn't support that kind of thing.
            # data = await conn.form()
            # if "access_token" in data:
            #     auth = "Bearer: " + data.get("access_token")
            raise HTTPException(status_code=500)
        if auth is None:
            raise AuthenticationError("unauthorized")
        token = await self.check_token(auth)
        try:
            # Do I even need to have this?
            # It would be better to have safeguards about the domains that someone owns.
            # This would allow Kittybox to be multi-user.
            if token["me"] != conn.app.state.identity:
                raise ValueError
            return (
                AuthCredentials(["valid"] + token["scope"].split(" ")),
                SimpleUser(token["me"])
            )
        except Exception:
            raise AuthenticationError("forbidden")


@requires("valid", status_code=401)
async def micropub_query(request: Request) -> UJSONResponse:
    """Query Micropub for posts, configuration settings or more."""
    if "q" not in request.query_params:
        return UJSONResponse({"error": "invalid_request"}, status_code=400)
    if request.query_params["q"] == "geo":
        try:
            return UJSONResponse(
                await request.app.state.backend.georadius(
                    float(request.query_params["lat"]),
                    float(request.query_params["lon"])
                )
            )
        except KeyError:
            return UJSONResponse({"error": "invalid_request"}, status_code=400)
    if request.query_params["q"] == "config":
        return UJSONResponse({
            "media-endpoint": request.app.state.media_endpoint,
            "q": ["config", "source", "syndicate-to", "contact", "category", "food"],
            "syndicate-to": request.app.state.syndicate_to,
            "site-name": request.app.state.site_name,
        })
    if request.query_params["q"] == "source":
        if "url" in request.query_params:
            try:
                return UJSONResponse(
                    await request.app.state.backend.read_post(
                        request.query_params["url"]
                    )
                )
            except FileNotFoundError as exc:
                return UJSONResponse(
                    {"error": "not_found",
                     "error_description": str(exc)},
                    status_code=404
                )
        else:
            limit: int = 10
            before: Optional[str] = None
            after: Optional[str] = None
            filters: Dict[str, Union[str, List[str]]] = {}
            if "filters" in request.query_params:
                filters = parse_filter_query(request.query_params["filters"])
            if "before" in request.query_params:
                before = request.query_params["before"]
            if "after" in request.query_params:
                after = request.query_params["after"]
            if "limit" in request.query_params:
                limit = int(request.query_params["limit"])
            return UJSONResponse(
                await request.app.state.backend.get_page(limit,
                                                         before, after,
                                                         filters)
            )
    if request.query_params["q"] == "syndicate-to":
        return UJSONResponse(request.app.state.syndicate_to)
    if request.query_params["q"] == "category":
        return UJSONResponse({"categories": await request.app.state.backend.categories()})
    if request.query_params["q"] == "contact":
        return UJSONResponse({"contacts": await request.app.state.backend.contacts()})
    if request.query_params["q"] == "food":
        return UJSONResponse({"food": await request.app.state.backend.food()})
    raise HTTPException(status_code=400)


@requires("valid", status_code=401)
async def micropub_accept(request: Request) -> UJSONResponse:
    """Accept a Micropub request and dispatch it to a proper handler."""
    data: Union[FormData, dict]
    if request.headers["Content-Type"] in (
            "multipart/form-data", "application/x-www-form-urlencoded"):
        need_conversion = True
        data = await request.form()
    elif request.headers["Content-Type"] == "application/json":
        need_conversion = False
        data = await request.json()
    else:
        logging.getLogger(__name__).error("Invalid data type received: %s", request.headers["Content-Type"])
        return UJSONResponse({
            "error": "invalid_request",
            "error_description": "Invalid Content-Type. Accepting form submissions or JSON (application/json)."
        }, status_code=400)
    if data.get("action"):
        if need_conversion:  # Only (un)deletion is made with form-encoded
            data = {"action": data.get("action"), "url": data.get("url")}
        if data["action"] == "delete":
            return await micropub_delete(request, data)
        if data["action"] == "undelete":
            return await micropub_undelete(request, data)
        if data["action"] == "update":
            return await micropub_update(request, data)
        raise HTTPException(status_code=400)
    try:
        if need_conversion:
            realdata: MF2Object = convert_to_mf2object(cast(FormData, data))
        else:
            realdata = MF2Object(cast(dict, data))
    except ValueError as exc:
        return UJSONResponse({
            "error": "invalid_request",
            "error_description": str(exc)
        }, status_code=400)
    return await micropub_create(request, realdata)


def get_location_header(resp: aiohttp.ClientResponse) -> str:
    """Get a location header from ClientResponse."""
    return resp.headers["Location"]


async def write_post_task(request: Request, post: MF2Object, uid: str,
                          update: Optional[bool] = False) -> None:
    """Write a post to the backing store in the background."""
    if not update:
        if not hasattr(post, "uid"):
            post.uid = [uid]
        if not hasattr(post, "url"):
            post.url = [uid]
        post.url.extend(
            generate_additional_urls(post, request.app.state.identity)
        )
        # Deduplicate URLs in case stuff got generated
        post.url = list(set(post.url))
        if uid not in post.url:
            post.url.insert(0, uid)
        for i, url in enumerate(getattr(post, "checkin", [])):
            if url.startswith("geo:"):
                if len(url.split(";")) == 3:
                    geo, name, checkin = url.split(";")
                    try:
                        hcard = request.app.state.backend.read_post(checkin)
                    except FileNotFoundError:
                        hcard = MF2Object({
                            "type": ["h-card"], "properties": {
                                "name": [name], "published": [nowiso8601()],
                                "geo": [{
                                    "type": ["h-geo"], "properties": {
                                        "latitude": [
                                            geo.split(":")[1].split(",")[0]
                                        ], "longitude": [
                                            geo.split(":")[1].split(",")[1]
                                        ], "altitude": [
                                            geo.split(":")[1].split(",")[2]
                                        ]
                                    }
                                }]
                            }
                        })
                        card_uid = generate_url(hcard, request.app.state.identity)
                        hcard.url = [card_uid]
                        hcard.uid = [card_uid]
                        await request.app.state.backend.write_post(hcard, card_uid)
                        # Every single post creation and update should be posted to a message queue
                        await request.app.state.mq.post_msg("posts", {"old": None, "new": hcard})
                else:
                    geo, name = url.split(";")
                    checkin = await request.app.state.backend.georadius(
                        geo.split(":")[1].split(",")[0],
                        geo.split(":")[1].split(",")[1]
                    )
                    if checkin.get("geo") is None:
                        hcard = MF2Object({
                            "type": ["h-card"], "properties": {
                                "name": [name], "published": [nowiso8601()],
                                "geo": [{
                                    "type": ["h-geo"], "properties": {
                                        "latitude": [
                                            geo.split(":")[1].split(",")[0]
                                        ], "longitude": [
                                            geo.split(":")[1].split(",")[1]
                                        ], "altitude": [
                                            geo.split(":")[1].split(",")[2]
                                        ]
                                    }
                                }]
                            }
                        })
                        card_uid = generate_url(hcard, request.app.state.identity)
                        hcard.url = [card_uid]
                        hcard.uid = [card_uid]
                        await request.app.state.backend.write_post(hcard, card_uid)
                        request.app.state.mq.post_msg("posts", {"old": None, "new": hcard})
                    else:
                        hcard = request.app.state.backend.read_post(checkin)
                post.checkin[i] = hcard.uid[0]
    logging.getLogger(__name__).info("Writing -- first pass")
    await request.app.state.backend.write_post(post, post.uid[0])
    logging.getLogger(__name__).info("Sending the new post to the message queue for feed processing")
    await request.app.state.mq.post_msg("posts", {"old": None, "new": post.to_dict()})

    # After first write, we can syndicate the post
    sender = WebmentionSender(request.app.state.http_client)
    if hasattr(post, "mp-syndicate-to"):
        post.syndication = list(map(
            get_location_header, await sender.send_webmentions(
                post.uid[0], getattr(post, "mp-syndicate-to")
            )
        ))
        delattr(post, "mp-syndicate-to")
        # Rewrite post in the database after syndication
        logging.getLogger(__name__).info("Writing -- second pass")
        await request.app.state.backend.write_post(post, post.uid[0])
    # After this, we can send webmentions
    await sender.send_webmentions_for_post(post)


def generate_default_slug(data: MF2Object) -> str:
    """Generate a default slug for a post."""
    return numtosxg(int(dateutil.parser.parse(data.published[0]).timestamp()))


def get_postkey(data: MF2Object) -> str:
    """Get a key for generating a post URL.

    This method of storage assumes a URL structure of
    `https://$IDENTITY/$KEY/$POSTID` and mostly used in legacy versions of
    kittybox."""
    if data.type[0] == "h-entry":
        postkey = "posts"
    else:
        postkey = data.type[0].replace("-", "") + "s"
    return postkey


def generate_url(data: MF2Object, identity: str) -> str:
    """Generate a representative URL for this object."""
    return urllib.parse.urljoin(
        identity, get_postkey(data) + "/" + generate_default_slug(data))


def generate_additional_urls(post: MF2Object, identity: str) -> List[str]:
    """Generate a list of all URLs for this object, including URLs with custom
    slugs and such."""
    postkey = get_postkey(post)
    return [urllib.parse.urljoin(identity, postkey + "/" + slug)
            if not slug.startswith("/") else urllib.parse.urljoin(identity, slug)
            for slug in getattr(post, "mp-slug", [])]


def nowiso8601() -> str:
    """Return current date and time in ISO 8601 format.

    Timezone is determined from timedelta config value."""
    return datetime.datetime.now(
        tz=datetime.timezone(datetime.timedelta(
            hours=(time.localtime().tm_gmtoff / 3600)
        ))
    ).isoformat(timespec="seconds")


@requires("create", status_code=401)
async def micropub_create(request: Request, data: MF2Object) -> UJSONResponse:
    """Check for correctness, place a background task and return post URL."""
    if not data.type[0].startswith("h-"):
        return UJSONResponse({"error": "invalid_request"}, status_code=400)
    if not hasattr(data, "published"):
        data.published = [nowiso8601()]
    if not hasattr(data, "visibility"):
        data.visibility = ["public"]
    if not hasattr(data, "author") and "h-card" not in data.type:
        data.author = [request.user.display_name]

    if hasattr(data, "uid"):
        uid = data.uid[0]
    else:
        uid = generate_url(data, request.app.state.identity)
    logging.getLogger(__name__).info("Spawning background task to save the post %s", uid)
    return UJSONResponse(
        {"url": uid}, status_code=202,
        headers={"Location": uid},
        background=BackgroundTask(write_post_task, request=request,
                                  post=data, uid=uid)
    )


@requires("update", status_code=401)
async def micropub_update(request: Request, data: dict) -> Response:
    """Update a MF2 object using `data` as Micropub update expression."""
    logging.getLogger(__name__).debug("Post update data: %s", data)
    try:
        post: dict = await request.app.state.backend.read_post(data["url"])
    except (KeyError, FileNotFoundError):
        return UJSONResponse({
            "error": "invalid_request",
            "error_description": "Post wasn't found in the database" +
                                 "or wasn't specified."
        }, status_code=400)
    # Save a copy of the old post
    oldpost = copy.deepcopy(post)
    for entry in data.get("add", {}):
        if entry == "children":
            post[entry] = cast(List, data[entry]) + post.get(entry, [])
        else:
            post["properties"].setdefault(entry, []).extend(data["add"][entry])
    if isinstance(data.get("remove"), list):
        for entry in data["remove"]:
            try:
                if entry == "children":
                    del post[entry]
                else:
                    del post["properties"][entry]
            except KeyError:
                continue
    elif isinstance(data.get("remove"), dict):
        for entry in data["remove"]:
            for each in data["remove"].get(entry, []):
                try:
                    if entry == "children":
                        prop = post.get("children", [])
                    else:
                        prop = post["properties"].get(entry, [])
                    prop.remove(each)
                except ValueError:
                    pass
                finally:
                    val = post["properties"].get(entry)
                    if val is not None and len(val) == 0:
                        del post["properties"][entry]

    for entry in data.get("replace", []):
        prop = post["properties"]
        if entry == "children":
            prop = post
        prop[entry] = data["replace"][entry]

    mf2post = MF2Object(post)

    await request.app.state.backend.write_post(mf2post, uid=post["properties"]["uid"][0])
    # Send webmentions for the post
    await WebmentionSender(request.app.state.http_client).send_webmentions_for_post(mf2post)
    request.app.state.mq.post_msg("posts", {"old": oldpost, "new": post, "update": data})
    return Response(status_code=204)


@requires("delete")
async def micropub_delete(request: Request, data: dict) -> Response:
    """Mark a specified post as deleted."""
    data = {
        "action": "update",
        "url": data["url"],
        "add": {
            "deleted": [nowiso8601()]
        }
    }
    return await micropub_update(request, data)


@requires("delete")
async def micropub_undelete(request: Request, data: dict) -> Response:
    """Unmark a specified post as deleted."""
    data = {
        "action": "update",
        "url": data["url"],
        "remove": ["deleted"]
    }
    return await micropub_update(request, data)


# pylint: disable=unused-argument
def auth_error(request: Request,
               exception: AuthenticationError) -> UJSONResponse:
    """Handle authentication errors for IndieAuth."""
    if str(exception) == "forbidden":
        return UJSONResponse({
            "error": "forbidden",
            "error_description": "The issuer of this token is not allowed to"
                                 + "authorize this operation."},
                             status_code=403)
    return UJSONResponse({"error": str(exception)}, status_code=401)


CONFIG: Config = None  # type: ignore


def get_app() -> Starlette:
    """Get a configured application instance."""
    async def start_aiohttp_session() -> None:
        """Start an AIOHTTP session that allows to connect to the
        authentication endpoint."""
        if not hasattr(app.state, "http_client"):
            app.state.http_client = aiohttp.ClientSession(
                headers={"Accept": "application/json"}
            )

    async def cleanup_aiohttp_session() -> None:
        """Close the remaining HTTP client session."""
        await app.state.http_client.close()

    async def connect_to_backing_store() -> None:
        """Make a connection to the backend for Micropub server."""
        backend_uri = app.state.backend_uri
        if backend_uri.startswith("redis://"):
            app.state.backend = RedisBackend(backend_uri)
        # elif backend_uri.startswith("mongodb://"):
        #    app.state.backend = MongoBackend(backend_uri)
        else:
            print("Warning: no backend configured.", file=sys.stderr)
            return
        await app.state.backend.__post_init__()

    async def connect_to_message_queue() -> None:
        mq_uri = app.state.message_queue_uri
        if mq_uri.startswith("redis://"):
            app.state.mq = RedisMessageQueue(mq_uri)
        else:
            print("Warning: no message queue configured.", file=sys.stderr)
            return
        await app.state.mq.connect()

    async def cleanup_backing_store() -> None:
        """Cleanup the connection to the backend store."""
        if hasattr(app.state, "backend"):
            await app.state.backend.close()

    def indiewebxyz(lang: str, sub: str) -> Dict[str, str]:
        """Return a syndication entry for indieweb.xyz sub"""
        return {
            "name": "indieweb.xyz/{}/{}".format(lang, sub),
            "uid": "https://indieweb.xyz/{}/{}".format(lang, sub)
        }

    config = cast(Config, Config(".env"))
    # This is needed for other functions in global scope to pick up the
    # configuration for kittybox.
    # pylint: disable=global-statement
    global CONFIG
    CONFIG = config
    debug: bool = config('DEBUG', cast=bool, default=False)
    app = Starlette(
        debug=debug,
        routes=[
            Route('/', micropub_accept, methods=["POST"]),
            Route('/', micropub_query, methods=["GET"]),
        ], middleware=[
            Middleware(AuthenticationMiddleware, backend=IndieAuthBackend(),
                       on_error=auth_error),
        ], exception_handlers={
            400: lambda r, e: UJSONResponse({
                "error": "invalid_request", "error_description": str(e)
            }, status_code=400)
        }
    )
    app.state.token_endpoint = config(
        'TOKEN_ENDPOINT', default="https://tokens.indieauth.com/token"
    )
    app.state.media_endpoint = config("MEDIA_ENDPOINT", default=None)
    app.state.backend_uri = config("BACKEND", default="")
    app.state.testing = config("TESTING", default=False, cast=bool)
    app.state.message_queue_uri = config("MESSAGE_QUEUE", default="")
    # TODO make this modifiable at runtime (store in backend?)
    # TODO make a frontend app and an API capable of modifying these params
    # and include it with the Kittybox distribution
    app.state.site_name = config("SITE_NAME", default="Vika's Hideout")
    app.state.identity = config("IDENTITY", default="https://fireburn.ru")
    app.state.syndicate_to = [
        {
            "name": "Twitter",
            "service": {
                "name": "Twitter",
                "photo": "https://fireburn.ru/media/1b/e5/d0/5c/e6faad469f7f9c5a5879f2d9f8d267b60eb394e92c19217268bcea8f.png",
                "url": "https://twitter.com"
            },
            "uid": "https://brid.gy/publish/twitter",
            "user": {
                "name": "kisik21",
                "photo": "https://fireburn.ru/media/f1/5a/fb/9b/081efafb97b4ad59f5025cf2fd0678b8f3e20e4c292489107d52be09.png",
                "url": "https://twitter.com/kisik21"
            }
        },
        {
            "name": "IndieNews (en)",
            "uid": "https://news.indieweb.org/en"
        },
        indiewebxyz("en", "rant"),
        indiewebxyz("en", "indieweb"),
        indiewebxyz("en", "trans"),
        indiewebxyz("en", "cats"),
        indiewebxyz("en", "dev"),
        indiewebxyz("en", "technology")
    ]
    outer_app = Starlette(
        routes=[
            Route('/metrics', metrics),
            Mount('/', app, name="micropub")
        ],
        middleware=[
            Middleware(PrometheusMiddleware),
            Middleware(CORSMiddleware,
                       allow_origins=['*'], allow_methods=['*'],
                       allow_headers=["*"],
                       expose_headers=["Location"]),
        ],
        on_startup=[
            start_aiohttp_session,
            connect_to_backing_store,
            connect_to_message_queue
        ],
        on_shutdown=[
            cleanup_aiohttp_session,
            cleanup_backing_store
        ]
    )
    return outer_app
