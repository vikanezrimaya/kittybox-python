"""Utility library for working with MF2 Objects."""
from __future__ import annotations
import copy
from typing import Dict, List, Union, Any
import ujson
from starlette.datastructures import FormData


class MF2Object:
    """An arbitrary MF2 object. This is provided for usability reasons.

    Querying is simplified. obj.type returns types that this object conforms
    to. Everything else is retrieved from properties. Inner MF2-JSON objects
    are also converted.

    Use obj.to_json() to receive your MF2-JSON dict back."""

    def __init__(self, json: dict):
        try:
            print(json)
            self.__dict__["_type"] = json["type"]
            self.__dict__["properties"] = {}
            self.__dict__["graft"] = {}
            for prop in json["properties"].keys():
                for i in json["properties"][prop]:
                    if isinstance(i, dict) and "type" in i:
                        self.properties.setdefault(prop, []).append(MF2Object(i))
                    else:
                        self.properties.setdefault(prop, []).append(i)
            j = copy.copy(json)
            del j["type"]
            del j["properties"]
            if len(j) > 0:
                self.__dict__["graft"] = j
        except KeyError:
            raise ValueError("Incorrect MF2-JSON: {}".format(ujson.dumps(json)))

    def __repr__(self) -> str:
        return "MF2Object({})".format(self.to_json())

    def __getattr__(self, name: str) -> Any:
        if name == "type":
            return self._type
        val = self.properties.get(name)
        if val is None:
            raise AttributeError
        return val

    def __setattr__(self, name: str, value: List[Union[str, MF2Object]]) -> None:
        self.__dict__["properties"][name] = value

    def __delattr__(self, name: str) -> None:
        del self.__dict__["properties"][name]

    def to_dict(self) -> dict:
        """Convert a MF2Object into JSON-like dictionary.

        This method guarantees that an expression
        `MF2Object(data).to_dict() == data` is true, given that `data` is
        a valid MF2-JSON object."""
        data = {
            "type": self._type,
            "properties": {}
        }
        for prop in self.properties:
            for i in self.properties[prop]:
                if isinstance(i, MF2Object):
                    data["properties"].setdefault(prop, []).append(i.to_dict())
                else:
                    data["properties"].setdefault(prop, []).append(i)
        data.update(self.graft)
        return data

    def to_json(self) -> str:
        """Dump into a JSON string.

        This method guarantees that an expression
        `json.loads(MF2Object(data).to_json()) == data is `True` if the
        expression `json.loads(json.dumps(data)) == data` is `True`."""
        return ujson.dumps(self.to_dict())


def convert_to_mf2object(form: FormData) -> MF2Object:
    """Convert a form-encoded post into a MF2Object instance."""
    data: Dict[str, Union[Dict[str, list], list]] = {}
    for prop in form:
        if prop == "access_token":
            continue
        if prop == "h":
            data["type"] = [f"h-{form.get(prop)}"]
        else:
            data.setdefault("properties", dict())[prop] = form.getlist(prop)
    return MF2Object(data)
