"""newbase60.py - Python implementation of Tantek's NewBase60

http://tantek.pbworks.com/w/page/19402946/NewBase60

By Kevin Marks, ported to Python 3 and improved with type annotations
by Vika Shleina. Licensed under CC0.

Original code at: https://github.com/indieweb/newBase60py
"""

CHARACTERS = '0123456789ABCDEFGHJKLMNPQRSTUVWXYZ_abcdefghijkmnopqrstuvwxyz'

NUMBERS = dict(zip(CHARACTERS, range(len(CHARACTERS))))
NUMBERS['l'] = 1  # typo lowercase l to 1
NUMBERS['I'] = 1  # typo capital I to 1
NUMBERS['O'] = 0  # typo capital O to 0


def numtosxg(num: int) -> str:
    """Convert an int into a NewBase60 numeral."""
    sxg = ''
    if not isinstance(num, int) or num == 0:
        return '0'
    while num > 0:
        num, i = divmod(num, 60)
        sxg = CHARACTERS[i] + sxg
    return sxg


def sxgtonum(sxg: str) -> int:
    """Convert a NewBase60 numeral into an int."""
    num = 0
    for character in sxg:
        num = num*60+NUMBERS.get(character, 0)
    return num
