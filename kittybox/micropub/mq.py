"""Message queues that are used to delegate background tasks to external services."""
from typing import Any
from abc import ABC, abstractmethod
import aioredis
import ujson

class MessageQueue(ABC):
    """Abstract message queue adapter."""
    @abstractmethod
    def __init__(self, uri: str) -> None:
        """Initialize the queue."""

    async def connect(self) -> None:
        """Connect to the queue."""

    async def post_msg(self, queue: str, msg: Any) -> None:
        """Post a message `msg`, serialized as JSON, to a queue."""

    async def get_msg(self, queue: str) -> Any:
        """Get a message from a queue."""

    async def close(self) -> None:
        """Disconnect from the queue."""

class RedisMessageQueue(MessageQueue):
    """Redis as a message queue."""
    # pylint: disable=super-init-not-called
    def __init__(self, uri: str) -> None:
        self.uri: str = uri
        self.redis: aioredis.Redis

    async def connect(self) -> None:
        self.redis = await aioredis.create_redis_pool(self.uri,
                                                      encoding="utf-8")

    async def post_msg(self, queue: str, msg: Any) -> None:
        await self.redis.lpush(f"mq:{queue}", ujson.dumps(msg))

    async def get_msg(self, queue: str) -> None:
        return await ujson.loads(await self.redis.blpop(f"mq:{queue}"))

    async def close(self) -> None:
        self.redis.close()
        return await self.redis.wait_closed()
