"""Kittybox runner."""
import starlette
import os
import asyncio
import signal
from typing import Any, List, NoReturn
import uvloop
from hypercorn.config import Config
from hypercorn.asyncio import serve
from kittybox.micropub import get_app

def run() -> None:
    """Run kittybox with default configs."""
    config: Config = Config()
    config.bind = ["0.0.0.0:8000"]
    if os.getenv("DEBUG"):
        config.bind = ["127.0.0.1:8000"]
    app: starlette.applications.Starlette = get_app()
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    shutdown_event: asyncio.Event = asyncio.Event()

    def _signal_handler(*_: Any) -> None:
        shutdown_event.set()

    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGTERM, _signal_handler)
    loop.add_signal_handler(signal.SIGINT, _signal_handler)
    # This code is perfectly valid. I don't know why mypy complains.
    loop.run_until_complete(
        serve(app, config, shutdown_trigger=shutdown_event.wait)  # type: ignore
    )

run()
