"""Collection of backends for kittybox."""
from typing import List, Dict, Union, Callable, Optional, Any
from abc import ABC, abstractmethod
import asyncio
from asyncio import Future
import ujson
from kittybox.micropub.mf2object import MF2Object
try:
    import aioredis
except ImportError:  # pragma: no cover
    aioredis = None
#
#try:
#    import motor
#except ImportError:  # pragma: no cover
#    motor = None


class Backend(ABC):
    """Null backend that does nothing and cannot be instantiated."""
    @abstractmethod
    def __init__(self, uri: str):
        """Initialise the backend using the given URI."""

    @abstractmethod
    async def __post_init__(self) -> None:
        """Gets called after initialization."""

    @abstractmethod
    async def write_post(self, post: MF2Object, uid: str) -> None:
        """Write the post to an underlying store, using `uid` as a key."""

    @abstractmethod
    async def read_post(self, url: str) -> dict:
        """Read the post from an underlying store."""

    @abstractmethod
    async def get_page(self, limit: int = 10,
                       before: Optional[str] = None,
                       after: Optional[str] = None,
                       filters: Optional[
                           Dict[str, Union[List[str], str]]
                       ] = None) -> dict:
        """Get a page of posts."""

    @abstractmethod
    async def categories(self) -> List[str]:
        """Get all categories that are assigned to posts existing in this server."""

    @abstractmethod
    async def contacts(self) -> List[dict]:
        """Get a list of contacts suitable for ?q=contact.

        These are probably good people if you're listing them at your website,
        which you essentially do."""

    @abstractmethod
    async def food(self) -> List[dict]:
        """Get a list of items suitable for human consumption described at this server.

        Not guaranteed to sort things by tastiness nor by freshness! :3"""

    @abstractmethod
    async def close(self) -> None:
        """Close all connections and cleanup."""

    @abstractmethod
    async def georadius(self, lat: float, lon: float) -> dict:
        """Return a bunch of venues in your location."""


class RedisBackend(Backend):
    """Backend storing objects as items of a Redis hash.

    It also maintains several indexes for posts to be found."""
    # pylint: disable=super-init-not-called
    def __init__(self, uri: str):
        self.uri: str = uri
        self.redis: aioredis.Redis

    async def __post_init__(self) -> None:
        self.redis = await aioredis.create_redis_pool(self.uri,
                                                      encoding="utf-8")

    async def _convert_geomember(self, i: aioredis.GeoMember) -> dict:
        return {
            "label": (await self.read_post(i.member))["properties"]["name"][0],
            "latitude": i.coord.latitude,
            "longitude": i.coord.longitude,
            "url": i.member
        }

    async def georadius(self, lat: float, lon: float) -> dict:
        results: List[aioredis.GeoMember] = await self.redis.georadius(
            "geo", lon, lat, 100, "m", with_coord=True, sort="ASC")
        output: List[dict] = await asyncio.gather(*list(map(self._convert_geomember, results)))
        geo = output[0] if len(output) > 0 else None
        return {
            "geo": geo,
            "venues": output
        }

    def _gather_index_difference(self, post: MF2Object, oldpost: MF2Object,
                                 url: str, prop: str, prefix: str,
                                 remove: bool = False,
                                 helper: Optional[Callable] = None) -> Future:
        old = set(getattr(oldpost, prop, []))
        new = set(getattr(post, prop, []))
        if helper:
            oldprom = helper(
                url, list(new.difference(old)),
                seed="feed__protected" if prefix == "feed" else "feed"
            )
        else:
            oldprom = asyncio.gather(*[
                self.redis.lpush(f"{prefix}_{user}", url)
                for user in new.difference(old)
            ])
        if remove:
            newprom = asyncio.gather(*[
                self.redis.lrem(f"{prefix}_{user}", 0, url) for user
                in old.difference(new)
            ])
            return asyncio.gather(oldprom, newprom)
        return oldprom

    async def _init_user_timeline_maybe(
            self, uri: str, users: List[str],
            seed: str = "feed__protected") -> Future:
        protected_feed = await self.redis.lrange(seed, 0, -1)
        return asyncio.gather(*[
            self._populate_timeline_maybe(uri, user, protected_feed)
            for user in users
        ])

    async def _populate_timeline_maybe(self, uri: str, user: str,
                                       protected_feed: List[str]) -> None:
        if not await self.redis.exists(f"feed_{user}"):
            self.redis.lpush(f"feed_user", *protected_feed)
        self.redis.lpush(f"feed_{user}", uri)

    async def write_post(self, post: MF2Object, uid: str) -> None:
        update = await self.redis.hexists("objects", uid)
        self.redis.hset("objects", uid, post.to_json())
        for url in post.url:
            if url != uid:
                self.redis.hset("objects", url, ujson.dumps({"seeother": uid}))
        if not update:
            # This section adds posts to arrays when they are of certain type.
            # That helps with special views like ?q=contacts
            self.redis.lpush("objects_order", uid)
            if "h-card" in post.type:
                self.redis.lpush("contacts", uid)
                if hasattr(post, "geo"):
                    self.redis.lpush("venues", uid)
                    for geo in post.geo:
                        self.redis.geoadd("geo", geo.longitude[0], geo.latitude[0], post.uid[0])
            if hasattr(post, "category"):
                for cat in post.category:
                    await self.redis.zincrby("categories", 1, cat.lower())
            if "h-food" in post.type:
                self.redis.lpush("food", uid)

    async def categories(self) -> List[str]:
        return await self.redis.zrangebyscore("categories")

    async def _create_contact(self, contact: str) -> dict:
        contact: dict = await self.read_post(contact)
        return {
            "name": contact["properties"].get("name", [None])[0],
            "photo": contact["properties"].get("photo", [None])[0],
            "url": contact["properties"].get("url", [None])[0],
            "nickname": contact["properties"].get("nickname", [contact["properties"]["uid"][0].split("/")[-1]])[0],
            "_internal_url": contact["properties"]["uid"][0]
        }

    async def contacts(self) -> List[dict]:
        return await asyncio.gather(*list(map(self._create_contact, await self.redis.lrange("contacts", 0, -1))))

    async def read_post(self, url: str) -> dict:
        data: Optional[str] = await self.redis.hget("objects", url)
        if data is None:
            raise FileNotFoundError(f"Post {url} not found.")
        post: dict = ujson.loads(data)
        if "seeother" in post:
            return await self.read_post(post["seeother"])
        return post

    async def food(self) -> List[dict]:
        return await asyncio.gather(*list(map(self.read_post, await self.redis.lrange("food", 0, -1))))

    async def get_page(self, limit: int = 10,
                       before: Optional[str] = None,
                       after: Optional[str] = None,
                       filters: Optional[
                           Dict[str, Union[List[str], str]]
                       ] = None) -> dict:
        data: dict = {
            "items": []
        }
        feed = "objects_order"
        postlist = await self.redis.lrange(feed, 0, -1)
        if after:
            beginning = postlist.index(after)
            end = beginning + limit
        elif before:
            end = postlist.index(before)
            beginning = end - limit
        else:
            beginning = 0
            end = limit
        if beginning < 0:
            beginning = 0

        # Apply the limits to get the current page
        filtered = postlist[beginning:end]
        data["items"] = await asyncio.gather(*list(map(self.read_post, filtered)))
        if len(filtered) > limit:
            data.setdefault("paging", {})["after"] = filtered[-1]
        if beginning > 0:
            data.setdefault("paging", {})["before"] = filtered[0]
        return data

    async def close(self) -> Any:
        self.redis.close()
        return await self.redis.wait_closed()


#class MongoBackend(Backend):
#    """Backend for MongoDB."""
