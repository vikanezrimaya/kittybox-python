"""Utilities for Webmention sending."""
import asyncio
import functools
from enum import Enum, auto
from typing import List, Set, Union, Optional, Tuple, cast
from html.parser import HTMLParser
import aiohttp
from aiohttp import ClientResponse
from kittybox.micropub import MF2Object


class LinkTypes(Enum):
    """Types of links encountered and processed with LinkParser."""
    LINK = auto()
    WEBMENTION = auto()


# pylint: disable=abstract-method
class LinkParser(HTMLParser):
    """HTML parser that looks for webmention endpoints and mention links."""

    def __init__(self) -> None:
        super().__init__()
        self.webmention_endpoints: Set[str] = set()
        self.links: Set[str] = set()

    def handle_starttag(self, tag: str,
                        attrs: List[Tuple[str, Optional[str]]]) -> None:
        linktype: Optional[LinkTypes] = None
        href: Optional[str] = None
        if tag == "a":
            linktype = LinkTypes.LINK
        for k, v in attrs:
            if k == "rel" and v == "webmention":
                linktype = LinkTypes.WEBMENTION
            if k == "href":
                href = v
            if k == "rel" and v == "nofollow":
                linktype = None
        if linktype == LinkTypes.LINK:
            self.links.add(cast(str, href))
        elif linktype == LinkTypes.WEBMENTION:
            self.webmention_endpoints.add(cast(str, href))


class WebmentionSender:
    """Webmention sender class."""

    def __init__(self,
                 http_session: Optional[aiohttp.ClientSession] = None) -> None:
        # This is a lazily instantiated object.
        # Creating aiohttp.ClientSession objects outside an event loop
        # is deprecated. Thus we will instantiate it lazily in case we
        # don't have it instantiated.
        self.http_session: aiohttp.ClientSession = http_session  # type: ignore

    async def send_webmentions(self, source: str,
                               targets: List[str]) -> List[ClientResponse]:
        """Send webmentions for `source` to list of `targets`.

        Returns a compound future with ClientResponse instances for every
        sent webmention."""
        if self.http_session is None:
            self.http_session = aiohttp.ClientSession()
        return await asyncio.gather(*list(map(
            functools.partial(self.send_single_webmention, source), targets
        )), return_exceptions=True)

    async def send_single_webmention(self, source: str,
                                     target: str) -> ClientResponse:
        """Send a single webmention from source to target."""
        if self.http_session is None:
            self.http_session = aiohttp.ClientSession()
        return await self.http_session.post(
            await self.get_webmention_endpoint(target),
            data={"source": source, "target": target}
        )

    async def send_webmentions_for_post(self, post: MF2Object) -> List[
            ClientResponse]:
        """Send webmentions for every link in `post`."""
        webmention_targets: Set[str] = set()
        for field in {"reply-to", "like-of", "repost-of", "bookmark-of", "checkin"}:
            webmention_targets.update(
                map(self._get_post_uid, getattr(post, field, []))
            )
        parser = LinkParser()
        if hasattr(post, "content"):
            parser.feed(post.content[0]["html"]
                        if isinstance(post.content[0], dict) else post.content[0])
        if hasattr(post, "note"):
            parser.feed(post.note[0]["html"]
                        if isinstance(post.note[0], dict) else post.content[0])
        webmention_targets.update(parser.links)

        return await self.send_webmentions(post.uid[0],
                                           list(webmention_targets))

    @staticmethod
    def _get_post_uid(post: Union[str, dict]) -> str:
        if isinstance(post, str):
            return post
        if "uid" in post["properties"]:
            return post["properties"]["uid"][0]
        if "url" in post["properties"]:
            return post["properties"]["url"][0]
        raise ValueError("Cannot get representative URL for post")

    async def get_webmention_endpoint(self, target: str) -> str:
        """Find webmention endpoint for URL."""
        if self.http_session is None:
            self.http_session = aiohttp.ClientSession()
        response = await self.http_session.get(target)
        body = await response.text()
        parser = LinkParser()
        parser.feed(body)
        return list(parser.webmention_endpoints)[0]
