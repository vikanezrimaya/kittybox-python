"""Parses Kittybox filter queries, written in KittyQL.

Note: KittyQL is a proprietary feature of Kittybox and is not part of
Micropub specification."""
from typing import Dict, Union, List


def parse_filter_query(filters: str) -> Dict[str, Union[str, List[str]]]:
    """Parse a filter query."""
    filter_list = filters.split(";")
    filter_dict: Dict[str, Union[str, List[str]]] = {}
    for each in filter_list:
        command, arguments = each.split("(", maxsplit=2)
        arguments = arguments.strip(")")
        if command == "AUTH":
            filter_dict["auth"] = arguments
        elif command == "TAGS":
            filter_dict["tags"] = arguments.split(",")
        elif command == "TYPE":
            filter_dict["type"] = arguments

    return filter_dict
