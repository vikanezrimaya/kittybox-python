{ config, pkgs, lib, ... }:
{
  nixpkgs.overlays = [
    (import nix/overlay.nix)
  ];
  systemd.services.kittybox = {
    description = "Micropub endpoint";
    after = ["network-online.target"];
    serviceConfig = {
      User = "kittybox";
    };
    environment = {
      IDENTITY = "https://fireburn.ru/";
      TOKEN_ENDPOINT = "https://tokens.indieauth.com/token";
      BACKEND = "redis://localhost";
      MEDIA_ENDPOINT = "https://fireburn.ru/upload_media";
    };
    script = "exec ${pkgs.python38.withPackages (p: with p; [kittybox-micropub])}/bin/python3 -m kittybox.micropub";
  };
  services.nginx.virtualHosts."fireburn.ru".locations."/micropub" = {
    extraConfig = ''
      if ($request_method = 'OPTIONS') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
        add_header 'Access-Control-Max-Age' 1728000;
        add_header 'Content-Type' 'text/plain; charset=utf-8';
        add_header 'Content-Length' 0;
        return 204;
      }
      if ($request_method = 'POST') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range,Location';
      }
      if ($request_method = 'GET') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,Authorization';
        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range,Location';
      }
    '';
    proxyPass = "http://localhost:8000";
  };
  users.users.kittybox = {
    isNormalUser = false;
    home = "/var/empty";
  };
}
