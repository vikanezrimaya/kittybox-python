FROM python:3.7-slim-buster

# Default settings
# Setting them takes time. We make them here
ENV BACKEND="redis://redis:6379" IDENTITY="https://fireburn.ru/" TIMEDELTA=3 TOKEN_ENDPOINT="https://tokens.indieauth.com/token"
EXPOSE 8000

RUN useradd -M -u 10000 kittybox

#RUN apk add --virtual=.build-deps --no-cache gcc g++ musl-dev libffi-dev make
COPY . /src
RUN pip install "/src[redis]" && \
    rm -rf /root/.cache && \
    rm -rf /src
#RUN apk del .build-deps

USER kittybox

CMD ["python", "-m", "kittybox.micropub"]
