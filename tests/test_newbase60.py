import pytest
import multiprocessing
from kittybox.micropub.newbase60 import numtosxg, sxgtonum


def test_spots_to():
    assert numtosxg(0) == '0'
    assert numtosxg(1) == '1'
    assert numtosxg(60) == '10'


def test_spots_from():
    assert sxgtonum('0') == 0
    assert sxgtonum('1') == 1
    assert sxgtonum('10') == 60
    assert sxgtonum('NH') == 1337
    assert sxgtonum('l') == sxgtonum('I') == 1
    assert sxgtonum('O') == sxgtonum('|') == sxgtonum(',') == 0


def genrange(*args):
    if len(args) == 1:
        i = 0
        stop = args[0]
    elif len(args) == 2:
        i = args[0]
        stop = args[1]
    else:
        raise TypeError("genrange expected 1 or 2 arguments, got {}".format(len(args)))

    while (i < stop):
        yield i
        i += 1
    raise StopIteration()

# This is an insane test that takes a long time to complete.
# It checks conversion of all Unix times from 10^10 to 10^11.
# It is long. It can potentially hang your computer.
# Nobody should run this abomination ever.
@pytest.mark.slow
def test_cycle_conversion():
    for num in genrange(10**10, 10**11):
        assert sxgtonum(numtosxg(num)) == num
