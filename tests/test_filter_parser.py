from kittybox.micropub.filter_parser import parse_filter_query


def test_main_feed():
    assert parse_filter_query("TYPE(h-entry)") == {"type": "h-entry"}


def test_authenticated_feed():
    assert parse_filter_query("AUTH(https://fireburn.ru);TYPE(h-entry)") == {
            "type": "h-entry",
            "auth": "https://fireburn.ru"
        }


def test_authenticated_tagged_feed():
    assert parse_filter_query(
        "AUTH(https://fireburn.ru);TYPE(h-entry);" + "TAGS(cats,catsoftheindieweb)"
    ) == {
        "type": "h-entry",
        "auth": "https://fireburn.ru",
        "tags": ["cats", "catsoftheindieweb"]
    }
