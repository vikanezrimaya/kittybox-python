"""Tests for the Micropub endpoint, without the backend (tested separately).

These check how the endpoint responds to various requests, including incorrect ones."""
# pylint: disable=redefined-outer-name
import copy
import unittest.mock as mock
import pytest
from starlette.config import environ
from starlette.testclient import TestClient
from aioresponses import aioresponses
import kittybox.micropub
from kittybox.micropub.mf2object import MF2Object
from kittybox.micropub.backends import Backend
from kittybox.micropub.mq import MessageQueue

TOKEN_ENDPOINT = "https://token_endpoint.example.com"
environ["TOKEN_ENDPOINT"] = TOKEN_ENDPOINT
environ["IDENTITY"] = "http://localhost"
IDENTITY = environ["IDENTITY"]
MEDIA_AVAILABLE = bool(environ.get("MEDIA_ENDPOINT"))
environ["TESTING"] = "True"
VALID_POST = {
    "type": ["h-entry"],
    "properties": {
        "content": ["I am a post. I represent valid MF2-JSON format."],
        "category": ["awoo"]
    }
}
INVALID_POST = {
    "awoo": "I am an invalid post."
}
POST_WITH_URLS = {
    "type": ["h-entry"],
    "properties": {
        "content": ["I am a post. I already have a URL."],
        "url": ["https://fireburn.ru/stuff/life-stack"]
    },
}
HCARD_WITH_URL_AND_UID = {
    "type": ["h-card"],
    "properties": {
        "name": ["Vika"],
        "url": ["https://fireburn.ru/", "https://fireburn.ru/me",
                "https://twitter.com/kisik21"],
        "uid": ["https://fireburn.ru/me"]
    }
}

POST_WITH_INDIGENOUS_CHECKIN = {
    "type": ["h-entry"],
    "properties": {
        "content": ["Test content"],
        "checkin": ["geo:53.20090,45.01890,123.0000000;name=Антикафе Крылья"]
    }
}
SYNDICATION_SERVICE = "https://test.syndication.fake/"
SYNDICATION_WEBMENTION_ENDPOINT = "https://test.syndication.fake/webmention"
SYNDICATED_LINK = "https://test.syndication.fake/syndicated-post"
POST_WITH_SYNDICATION = {
    "type": ["h-entry"],
    "properties": {
        "content": ["Awoo"],
        "mp-syndicate-to": [SYNDICATION_SERVICE]
    }
}
def gen_update_add(url):
    return {
        "action": "update",
        "url": url,
        "add": {
            "category": ["nya"],
            "syndication": ["https://twitter.com/tweet/123"]
        }
    }


UPDATED_ADD = copy.deepcopy(VALID_POST)
UPDATED_ADD["properties"]["category"] = copy.deepcopy(
    VALID_POST["properties"]["category"])
UPDATED_ADD["properties"]["category"].append("nya")
UPDATED_ADD["properties"]["syndication"] = ["https://twitter.com/tweet/123"]


def gen_update_replace(url):
    """Generate an update payload for a given URL, replacing the "category" field."""
    return {
        "action": "update",
        "url": url,
        "replace": {
            "category": ["nya"]
        }
    }


UPDATED_REPLACE = copy.deepcopy(VALID_POST)
UPDATED_REPLACE["properties"]["category"] = ["nya"]


def gen_update_delete(url):
    """Generate an update for a given URL, deleting two fields."""
    return {
        "action": "update",
        "url": url,
        "remove": ["category", "awoo"]
    }


UPDATED_DELETE = copy.deepcopy(VALID_POST)
del UPDATED_DELETE["properties"]["category"]


def gen_update_delete_dict(url):
    """Generate an update payload, deleting two categories and one value from a custom field."""
    return {
        "action": "update",
        "url": url,
        "remove": {
            "category": ["woof", "awoo"],
            "awoo": ["nya"]
        }
    }


async def async_magic(value=None):
    """Do async magic. Consider it a Promise.resolve() for Python."""
    return value


mock.MagicMock.__await__ = lambda x: async_magic().__await__()

POST_WITH_MP_SLUG = MF2Object({
    "type": ["h-entry"],
    "properties": {
        "mp-slug": ["slug"]
    }
})
POST_WITH_MP_SLUG_SLASHED = MF2Object({
    "type": ["h-entry"],
    "properties": {
        "mp-slug": ["/slug"]
    }
})
def test_mp_slug():
    ident = "https://fireburn.ru/"
    urls = kittybox.micropub.generate_additional_urls(POST_WITH_MP_SLUG, ident)
    assert (ident + "posts/slug") in urls

def test_mp_slug_slashed():
    ident = "https://fireburn.ru/"
    urls = kittybox.micropub.generate_additional_urls(POST_WITH_MP_SLUG_SLASHED, ident)
    assert (ident + "slug") in urls

@pytest.fixture
def client():
    """Return a test client for the Starlette framework, bound to the Micropub app."""
    return TestClient(kittybox.micropub.get_app())


@pytest.fixture
def aiohttp_mock():
    """Return an aiohttp mock, allowing us to mock remote responses."""
    with aioresponses() as mock:
        yield mock


@pytest.fixture
def backend_mock(client):
    """Return a backend mock, allowing us to mock the internal state."""
    client.app.routes[-1].app.state.backend = mock.MagicMock(spec=Backend)
    return client.app.routes[-1].app.state.backend

@pytest.fixture
def mq_mock(client):
    client.app.routes[-1].app.state.mq = mock.MagicMock(spec=MessageQueue)
    return client.app.routes[-1].app.state.mq

def test_unauthorized_request(client):
    """Test that a client not providing a token is given HTTP 401."""
    with client:
        response = client.get("/")
    assert response.status_code == 401
    assert response.json()["error"] == "unauthorized"


def test_authorized_request(client, aiohttp_mock):
    """Test that an authorized client with proper scopes can access the endpoint."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create update media"
    })
    with client:
        response = client.get("/", headers={
            "Authorization": "Bearer " + IDENTITY
        })
    assert response.status_code != 401
    assert response.status_code != 403


def test_q_config(client, aiohttp_mock):
    """Test that the endpoint responds properly to ?q=config."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create update media"
    })
    with client:
        response = client.get("/?q=config", headers={
            "Authorization": "Bearer " + IDENTITY
        })
        assert response.status_code == 200
        body = response.json()
        assert "q" in body
        assert "media-endpoint" in body
        assert "syndicate-to" in body
        for each in body["syndicate-to"]:
            assert "uid" in each
            assert "name" in each

def test_syndicate_to(client, aiohttp_mock):
    """Test that the endpoint responds properly to ?q=syndicate-to."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create update media"
    })
    with client:
        response = client.get("/?q=syndicate-to", headers={
            "Authorization": "Bearer " + IDENTITY
        })
        assert response.status_code == 200
        body = response.json()
        for each in body:
            assert "uid" in each
            assert "name" in each


def test_insertion_formdata(client, aiohttp_mock, backend_mock, mq_mock):
    """Test a simple post insertion, verify that a backend call was made."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create update"
    })
    content = "awoo"
    with client:
        response = client.post("/", headers={
            "Authorization": "Bearer token"
        }, data={
            "h": "entry",
            "content": content,
        })
    assert response.status_code == 202
    assert backend_mock.write_post.called
    assert backend_mock.write_post.call_args[0][0].content[0] == content
    assert mq_mock.post_msg.called


def test_invalid_typed_mf2_json(client, aiohttp_mock):
    """Test inserting a payload that's not valid MF2-JSON."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create"
    })
    with client:
        response = client.post("/", headers={
            "Authorization": "Bearer token"
        }, json={
            "type": ["awoo"],
            "properties": {
                "awoo": ["awoo!"]
            }
        })
        assert response.status_code == 400
        assert response.json()["error"] == "invalid_request"
        print(response.json())


def test_gibberish(client, aiohttp_mock):
    """Test inserting something stupid instead of a MF2-JSON payload."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create"
    })
    with client:
        response = client.post("/", headers={
            "Authorization": "Bearer token",
            "Content-Type": "text/plain"
        }, data="awooooooooo")
        assert response.status_code == 400


@pytest.mark.xfail
def test_insertion_token_in_formdata(client, aiohttp_mock, backend_mock, mq_mock):
    """Test that the endpoint can successfully retrieve an access token in the body."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create update"
    })
    content = "awoo"
    with client:
        response = client.post("/",
                               data={
                                   "h": "entry",
                                   "content": content,
                                   "access_token": "token"
                               })
    assert response.status_code == 202
    assert backend_mock.write_post.called
    assert mq_mock.post_msg.called
    assert backend_mock.write_post.call_args[0][0]["properties"]["content"][0] == content
    assert "access_token" not in backend_mock.write_post.call_args[0][0]["properties"]

# pylint: disable=too-many-arguments
@pytest.mark.parametrize("identity,scope,post,httpcode,written", [
    # pylint: disable=bad-whitespace
    (IDENTITY,                    "create", VALID_POST,   202, True),
    (IDENTITY,                    "read",   VALID_POST,   401, False),
    ("https://cluelessperson.me", "create", VALID_POST,   403, False),
    (IDENTITY,                    "create", INVALID_POST, 400, False)
])
def test_insertion(client, aiohttp_mock, backend_mock, mq_mock,
                   identity, scope, post, httpcode, written):
    """Test insertion of a post in various circumstances:

    1. A valid post is created by an authorized client.
    2. A post is created by an authorized client with insufficient scopes.
    3. A post inserted by a rogue client with a valid token (but not allowed to use this endpoint)
    4. An invalid post gets created by an authorized client. Wait, we've had this before..."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": identity,
        "scope": scope
    })
    with client:
        response = client.post("/", json=post,
                               # The token endpoint response varies depending on inputs to the test
                               headers={"Authorization":
                                        "Bearer " + identity + scope})

    assert response.status_code == httpcode
    assert backend_mock.write_post.called == written
    assert mq_mock.post_msg.called == written


@pytest.mark.parametrize("identity,scope,post,update,updated", [
    (IDENTITY, "create update", VALID_POST, gen_update_add, UPDATED_ADD),
    (IDENTITY, "create update", VALID_POST, gen_update_replace, UPDATED_REPLACE),
    (IDENTITY, "create update", VALID_POST, gen_update_delete, UPDATED_DELETE),
    (IDENTITY, "create update", VALID_POST, gen_update_delete_dict, UPDATED_DELETE)
])
def test_update(client, aiohttp_mock, backend_mock, mq_mock,
                identity, scope, post, update, updated):
    """Test updating existing posts."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": identity,
        "scope": scope
    })
    with client:
        response = client.post("/", json=post,
                               headers={"Authorization": "Bearer "
                                        + identity + scope})
    assert response.status_code == 202
    assert backend_mock.write_post.called
    assert mq_mock.post_msg.called
    written_post = copy.deepcopy(
        backend_mock.write_post.call_args[0][0].to_dict())
    backend_mock.read_post.return_value = async_magic(written_post)
    backend_mock.reset_mock()
    mq_mock.reset_mock()
    with client:
        response2 = client.post("/", json=update(response.headers["Location"]),
                                headers={"Authorization": "Bearer "
                                         + identity + scope})
    assert response2.status_code == 204
    assert backend_mock.write_post.called
    assert mq_mock.post_msg.called
    post_dict = backend_mock.write_post.call_args[0][0].to_dict()
    # Remove autogenerated properties to guarantee a fair comparison
    del post_dict["properties"]["url"]
    del post_dict["properties"]["uid"]
    del post_dict["properties"]["visibility"]
    del post_dict["properties"]["published"]
    del post_dict["properties"]["author"]
    assert post_dict["properties"] == updated["properties"]


def test_update_children(client, aiohttp_mock, backend_mock, mq_mock):
    """Test updating children of h-feed. This is not standard, since the Micropub spec really wasn't intended for posting feeds."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY, "scope": "create update"
    })
    with client:
        post = client.post("/", json=VALID_POST, headers={"Authorization": "Bearer updater"})
        written_feed = {
            "type": ["h-feed"],
            "properties": {
                "name": ["Test feed"]
            },
            "children": [post.headers["Location"]]
        }
        feed = client.post("/", json=written_feed, headers={"Authorization": "Bearer updater"})
    assert post.status_code in [201, 202]
    assert feed.status_code in [201, 202]
    with client:
        post2 = client.post("/", json=VALID_POST, headers={"Authorization": "Bearer updater"})
    assert post2.status_code in [201, 202]
    backend_mock.reset_mock()
    written_feed["properties"]["uid"] = [feed.headers["Location"]]
    backend_mock.read_post.return_value = async_magic(written_feed)
    with client:
        update = client.post("/", json={
            "action": "update",
            "url": feed.headers["Location"],
            "replace": {
                "children": [post2.headers["Location"], post.headers["Location"]]
            }
        }, headers={"Authorization": "Bearer updater"})
    assert update.status_code == 204
    post_dict = backend_mock.write_post.call_args[0][0].to_dict()
    assert post2.headers["Location"] in post_dict["children"]
    assert post.headers["Location"] in post_dict["children"]

def test_post_indigenous_checkin_wo_url(client, aiohttp_mock, backend_mock, mq_mock):
    """Tests that checkins created in Indigenous format get assigned a URL of its own."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create update"
    })

    # pylint: disable=unused-argument
    async def empty_georadius(*args, **kwargs):
        return {"geo": None, "venues": []}

    backend_mock.georadius.side_effect = empty_georadius

    with client:
        response = client.post("/", json=POST_WITH_INDIGENOUS_CHECKIN,
                               headers={"Authorization": "Bearer token"})
    assert response.status_code == 202
    assert backend_mock.write_post.called
    # We're not syndicating here.
    assert backend_mock.write_post.call_count == 2
    assert mq_mock.post_msg.called
    assert mq_mock.post_msg.call_count == 2
    assert isinstance(backend_mock.write_post.call_args[0][0].checkin[0], str)
    assert backend_mock.write_post.call_args[0][0].checkin[0].startswith(
        "http")


def test_update_not_found(client, aiohttp_mock, backend_mock):
    """Test updating a non-existent post."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create update"
    })

    async def return_not_found(*args, **kwargs):
        raise FileNotFoundError

    backend_mock.read_post.side_effect = return_not_found
    with client:
        response = client.post("/", json=gen_update_add("https://fireburn.ru/posts/awoo"),
                               headers={"Authorization": "Bearer token"})
    assert response.status_code == 400
    out = response.json()
    assert out["error"] == "invalid_request"


def test_insertion_already_url(client, aiohttp_mock, backend_mock, mq_mock):
    """Test inserting a post with a URL already defined.

    This is useful in case you're migrating between endpoints.
    This allows you to pipe contents of one micropub endpoint's ?q=source to another."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create"
    })
    with client:
        response = client.post("/", json=POST_WITH_URLS,
                               headers={"Authorization": "Bearer token"})
        assert response.status_code == 202
        assert backend_mock.write_post.called
        assert mq_mock.post_msg.called
        write_args = backend_mock.write_post.call_args
        assert isinstance(write_args[0][0], MF2Object)
        assert write_args[0][1] == write_args[0][0].uid[0]


def test_insertion_url_and_uid_already_present(client, aiohttp_mock, backend_mock, mq_mock):
    """Test inserting a post with both URL and UID defined."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create"
    })
    with client:
        try:
            response = client.post("/", json=HCARD_WITH_URL_AND_UID,
                                   headers={"Authorization": "Bearer token"})
        except AttributeError as err:
            print(err.args)
            print(dir(err))
            raise err
        assert response.status_code == 202
        assert backend_mock.write_post.called
        assert mq_mock.post_msg.called
        write_args = backend_mock.write_post.call_args
        assert isinstance(write_args[0][0], MF2Object)
        assert write_args[0][0].uid[0] == HCARD_WITH_URL_AND_UID["properties"]["uid"][0]
        for url in HCARD_WITH_URL_AND_UID["properties"]["url"]:
            assert url in write_args[0][0].url


def test_not_found(client, aiohttp_mock, backend_mock):
    """Test querying for a non-existent page."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create"
    })

    async def return_not_found(*args, **kwargs):
        raise FileNotFoundError

    backend_mock.read_post.side_effect = return_not_found
    with client:
        response = client.get("/?q=source&url=https://fireburn.ru/should/404",
                              headers={"Authorization": "Bearer token"})
        assert response.status_code == 404


def test_deletion(client, aiohttp_mock, backend_mock, mq_mock):
    """Test deleting a post."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create update delete undelete"
    })

    with client:
        response = client.post("/", headers={
            "Authorization": "Bearer purger"
        }, json={
            "type": ["h-entry"],
            "properties": {
                "content": ["Nya!"],
            }
        })
        assert response.status_code == 202
        assert backend_mock.write_post.called
        assert mq_mock.post_msg.called
        backend_mock.read_post.return_value = async_magic(
            backend_mock.write_post.call_args[0][0].to_dict()
        )
        backend_mock.reset_mock()
        mq_mock.reset_mock()
        response2 = client.post("/", headers={
            "Authorization": "Bearer purger"
        }, data={
            "action": "delete",
            "url": response.headers["Location"]
        })
        assert response2.status_code == 204
        assert backend_mock.write_post.called
        assert mq_mock.post_msg.called
        assert hasattr(backend_mock.write_post.call_args[0][0], "deleted")

# pylint: disable=unused-argument
async def get_empty_page(*args, **kwargs):
    """Mocks a backend response with an empty database."""
    return {"items": []}


@pytest.mark.parametrize("identity,httpcode,get_page", [
    # pylint: disable=bad-whitespace
    (IDENTITY,                200, get_empty_page),
    ("https://evilhacker.me", 403, get_empty_page)])
def test_q_source(client, aiohttp_mock, backend_mock,
                  identity, httpcode, get_page):
    """Test that valid users are allowed to use ?q=source and invalid ones do not."""
    backend_mock.get_page = get_page
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": identity,
        "scope": "create"
    })
    with client:
        response = client.get("/?q=source",
                              headers={"Authorization": "Bearer " + identity})
    assert response.status_code == httpcode
    if httpcode == 200:
        body = response.json()
        assert "items" in body
    elif httpcode == 403:
        assert response.json()["error"] == "forbidden"


def test_q_geo(client, aiohttp_mock, backend_mock):
    """Test that a backend properly implementing AbstractBackend.georadius()
    can allow the endpoint to respond to ?q=geo requests."""
    # pylint: disable=unused-argument
    async def mock_georadius(lat=None, lon=None):
        return {"geo": {
            "label": "Anticafe",
            "latitude": "53.2", "longitude": "45.12",
            "url": "https://fireburn.ru/hcards/anticafe_krilya"
        }, "venues": []}
    backend_mock.georadius.side_effect = mock_georadius
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create"
    })
    with client:
        response = client.get(
            "/?q=geo&lat=53&lon=45.1", headers={"Authorization": "Bearer token"})
    assert response.status_code == 200
    body = response.json()
    assert "geo" in body
    assert "venues" in body


def test_syndication(client, aiohttp_mock, backend_mock, mq_mock):
    """Test that syndication causes a rewrite of the post."""
    aiohttp_mock.get(TOKEN_ENDPOINT, payload={
        "me": IDENTITY,
        "scope": "create"
    })
    aiohttp_mock.get(SYNDICATION_SERVICE, body=f"""<html><head>
      <link rel="webmention" href="{SYNDICATION_WEBMENTION_ENDPOINT}">
    </head></html>""")
    aiohttp_mock.post(SYNDICATION_WEBMENTION_ENDPOINT, status=201, headers={"Location": SYNDICATED_LINK})

    with client:
        response = client.post("/", json=POST_WITH_SYNDICATION,
                               headers={"Authorization": "Bearer token"})
        assert response.status_code == 202
        assert backend_mock.write_post.called
        assert backend_mock.write_post.call_count == 2
        assert mq_mock.post_msg.called
        write_args = backend_mock.write_post.call_args
        assert isinstance(write_args[0][0], MF2Object)
        assert write_args[0][1] == write_args[0][0].uid[0]
