import pytest
from unittest import mock
from aioresponses import aioresponses
from kittybox.micropub.mf2object import MF2Object
from kittybox.micropub.webmention import WebmentionSender, LinkParser


def gen_post_dict(_type: str, **kwargs: dict):
    return {
        "type": [_type],
        "properties": kwargs
    }


async def async_magic(argument=None):
    return argument


mock.MagicMock.__await__ = lambda x: async_magic().__await__()


@pytest.fixture
def aiohttp_mock():
    with aioresponses() as mock:
        yield mock


@pytest.fixture
def linkparser():
    return LinkParser()


@pytest.fixture
def sender():
    return WebmentionSender()


@pytest.mark.parametrize("post,result", [
    ("https://fireburn.ru/posts/hello", "https://fireburn.ru/posts/hello"),
    (gen_post_dict("h-entry", url=["https://fireburn.ru/posts/some-post"]),
     "https://fireburn.ru/posts/some-post"),
    (gen_post_dict("h-entry", uid=["https://fireburn.ru/posts/quick-note"]),
     "https://fireburn.ru/posts/quick-note")
])
def test_get_post_uid(post, result):
    assert WebmentionSender._get_post_uid(post) == result


@pytest.mark.asyncio
async def test_webmention_endpoint_getter(sender, aiohttp_mock):
    target = "https://fireburn.ru/"
    aiohttp_mock.get(target, body="""<html><head>
      <link rel="webmention" href="https://fireburn.ru/webmention">
    </head></html>""")
    endpoint = await sender.get_webmention_endpoint(target)
    assert endpoint == "https://fireburn.ru/webmention"


@pytest.mark.asyncio
async def test_send_webmention(sender, aiohttp_mock):
    target = "https://fireburn.ru/"
    endpoint = "https://fireburn.ru/webmention"
    aiohttp_mock.get(target, body="""<html><head>
      <link rel="webmention" href="{}">
    </head></html>""".format(endpoint))
    aiohttp_mock.post(endpoint, status=201)
    mentions = await sender.send_webmentions_for_post(MF2Object(gen_post_dict(
        "h-entry", uid=["https://vika.me/posts/1"],
        content=["<a href=\"{}\"></a>".format(target)])))
    assert all([resp.status == 201 for resp in mentions])


@pytest.mark.asyncio
async def test_nofollow(sender, aiohttp_mock):
    mentions = await sender.send_webmentions_for_post(MF2Object(gen_post_dict(
        "h-entry", uid=["https://vika.me/posts/1"],
        content=["<a href=\"https://example.com\" rel=\"nofollow\">"])))
    assert len(mentions) == 0
