from starlette.datastructures import FormData
from kittybox.micropub.mf2object import MF2Object, convert_to_mf2object


def test_simple_post():
    """Test with a simple MF2-JSON post."""
    content = "This is a post's content."
    data = {
        "type": ["h-entry"], "properties": {
            "content": [content]
        }
    }
    entry = MF2Object(data)
    assert entry.type[0] == "h-entry"
    assert entry.content[0] == content
    assert data == entry.to_dict()


def test_checkin_post():
    """Test that inner MF2 objects get converted too."""
    content = "This is a post's content."
    lat, lon = 54, 45
    data = {
        "type": ["h-entry"], "properties": {
            "content": [content],
            "checkin": [{
                "type": ["h-geo"],
                "properties": {
                    "latitude": [lat],
                    "longitude": [lon]
                }
            }]
        }
    }
    entry = MF2Object(data)
    assert entry.type[0] == "h-entry"
    assert isinstance(entry.checkin[0], MF2Object)
    assert entry.checkin[0].type[0] == "h-geo"
    assert entry.checkin[0].latitude[0] == lat
    assert entry.checkin[0].longitude[0] == lon
    assert entry.to_dict() == data


def test_post_explicit_html():
    content = {
        "html": "<p>I am the bone of my sword.</p>",
        "value": "I am the bone of my sword."
    }
    entry = MF2Object({
        "type": ["h-entry"],
        "properties": {
            "content": [content]
        }
    })
    assert entry.content[0] == content


def test_formdata_conversion():
    """Test that FormData gets correctly converted."""
    content = "I am the bone of my sword."
    form = FormData([("h", "entry"), ("content", content),
                     ("access_token", "token")])
    entry = convert_to_mf2object(form)
    assert entry.type[0] == "h-entry"
    assert entry.content[0] == content
    assert not hasattr(entry, "access_token")


def test_preservation_internal_properties():
    post = {
        "_id": "awoo",
        "type": ["h-entry"],
        "properties": {
            "content": ["Awoo"]
        }
    }
    entry = MF2Object(post)
    print(entry)
    assert entry.to_dict()["_id"] == "awoo"
