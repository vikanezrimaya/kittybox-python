"""Tests for the Redis database adapter."""
# pylint: disable=redefined-outer-name
import os
import importlib
import pytest
from kittybox.micropub.mf2object import MF2Object
from kittybox.micropub.backends import RedisBackend


POST = MF2Object({
    "type": ["h-entry"],
    "properties": {
        "content": ["I am the bone of my sword."],
        "category": ["IndieWeb", "testing"],
        "url": ["https://fireburn.ru/posts/test"],
        "visibility": ["public"]
    }
})
UPDATED_POST = MF2Object({
    "type": ["h-entry"],
    "properties": {
        "content": ["Awoo"],
        "url": ["https://fireburn.ru/posts/test"],
        "visibility": ["public"]
    }
})
POST_WITH_SEVERAL_URLS = MF2Object({
    "type": ["h-entry"],
    "properties": {
        "content": ["Awoo!!!"],
        "url": ["https://fireburn.ru/posts/stuff",
                "https://fireburn.ru/posts/more-stuff",
                "https://fireburn.ru/even-more-stuff/here"],
        "visibility": ["public"]
    }
})
VENUE_HCARD = MF2Object({
    "type": ["h-card"],
    "properties": {
        "name": ["Awoo"],
        "url": ["https://fireburn.ru/hcards/checkin"],
        "uid": ["https://fireburn.ru/hcards/checkin"],
        "photo": ["https://fireburn.ru/media/venue.png"],
        "geo": [{
            "type": ["h-geo"],
            "properties": {
                "latitude": ["53.2009"],
                "longitude": ["45.0189"]
            }
        }]
    }
})
TASTY_FOOD = MF2Object({
    "type": ["h-food"],
    "properties": {
        "name": ["Cheeseburger"],
        "url": ["https://fireburn.ru/foods/cheeseburger"]
    }
})

@pytest.fixture
async def backend():
    """Construct a Redis backend for testing."""
    try:
        # pylint: disable=unused-import
        importlib.import_module("aioredis")
        redis_uri = os.environ["REDIS_URI"]
    except (ImportError, KeyError):
        pytest.skip("Redis is required to run RedisBackend tests.")
    backend = RedisBackend(redis_uri)
    await backend.__post_init__()
    await backend.redis.flushall()
    yield backend
    await backend.close()


@pytest.mark.asyncio
async def test_read_write_post(backend):
    """Test writing a post and subsequently reading it."""
    await backend.write_post(POST, POST.url[0])
    post = await backend.read_post(POST.url[0])
    assert post == POST.to_dict()
    assert (await backend.get_page())["items"] == [POST.to_dict()]


@pytest.mark.asyncio
async def test_read_write_post_several_urls(backend):
    """Test writing a post with several URLs and then reading the post from each URL."""
    await backend.write_post(POST_WITH_SEVERAL_URLS, POST.url[0])
    for url in POST_WITH_SEVERAL_URLS.url:
        assert await backend.read_post(url) == POST_WITH_SEVERAL_URLS.to_dict()
    assert len((await backend.get_page())["items"]) == 1


@pytest.mark.asyncio
async def test_nonexistent(backend):
    """Test that the backend raises FileNotFoundError when a non-existent post is requested."""
    with pytest.raises(FileNotFoundError):
        await backend.read_post("https://fireburn.ru/should/raise/404")


@pytest.mark.asyncio
async def test_get_page(backend):
    """Test the format of get_page(), used for ?q=source."""
    page = await backend.get_page()
    assert "items" in page
    assert "paging" not in page
    assert len(page["items"]) == 0
    await backend.write_post(POST, POST.url[0])
    page = await backend.get_page()
    assert "items" in page
    assert "paging" not in page
    assert len(page["items"]) == 1


@pytest.mark.asyncio
async def test_update_post(backend):
    """Test updating posts (overwriting them with new content)."""
    await backend.write_post(POST, POST.url[0])
    # Write an updated document to the same URL
    await backend.write_post(UPDATED_POST, POST.url[0])
    post = await backend.read_post(POST.url[0])
    assert post["properties"]["content"][0] == UPDATED_POST.content[0]
    assert len((await backend.get_page())["items"]) == 1


@pytest.mark.asyncio
async def test_empty_georadius(backend):
    """Test the georadius functionality that might be provided by the backend."""
    georadius = await backend.georadius(53.5, 45.21)
    assert georadius["geo"] is None
    assert len(georadius["venues"]) == 0


@pytest.mark.asyncio
async def test_categories(backend):
    """Test that categories query works."""
    await backend.write_post(POST, POST.url[0])
    cats = await backend.categories()
    assert "testing" in cats
    assert len(cats) == 2


@pytest.mark.asyncio
async def test_food(backend):
    """Test that food items get into a food index."""
    await backend.write_post(TASTY_FOOD, TASTY_FOOD.url[0])
    food = await backend.food()
    assert len(food) == 1


@pytest.mark.asyncio
async def test_checkin(backend):
    """Test writing an h-card and then requesting a georadius around a nearby point."""
    await backend.write_post(VENUE_HCARD, VENUE_HCARD.url[0])
    georadius = await backend.georadius(53.2009, 45.0189)
    assert georadius["geo"]["label"] == VENUE_HCARD.name[0]
    assert georadius["geo"]["latitude"] == pytest.approx(float(VENUE_HCARD.geo[0].latitude[0]))
    assert georadius["geo"]["longitude"] == pytest.approx(float(VENUE_HCARD.geo[0].longitude[0]))

@pytest.mark.asyncio
async def test_contacts(backend):
    """Test that ?q=contact works."""
    await backend.write_post(VENUE_HCARD, VENUE_HCARD.url[0])
    contacts = await backend.contacts()
    assert len(contacts) == 1
    assert contacts[0]["name"] == VENUE_HCARD.name[0]
    assert contacts[0]["url"] == VENUE_HCARD.url[0]
    assert contacts[0]["photo"] == VENUE_HCARD.photo[0]
    assert contacts[0]["_internal_url"] == VENUE_HCARD.uid[0]
