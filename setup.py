from setuptools import setup, find_namespace_packages


setup(
    name='kittybox-micropub',
    version='0.1.0',
    long_description=__doc__,
    packages=find_namespace_packages(include=["kittybox.*", "kittybox.*"]),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'aiodns',
        'aiofiles',
        'aiohttp',
        'cchardet',
        'markdown',
        'mf2py',
        'py-memoize',
        'hypercorn',
        'python-dateutil',
        'python-multipart',
        'starlette>=0.13.0',
        'starlette-prometheus',
        'ujson',
        'uvloop',
    ],
    tests_require=['aioresponses', 'pytest-cov', 'pytest', 'pytest-asyncio'],
    extras_require={
        "redis": ["aioredis", "hiredis"],
        "mongodb": ["motor"]
    }
)
