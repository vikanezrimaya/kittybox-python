with import <nixpkgs> { overlays = [(import nix/overlay.nix)]; }; kittybox-micropub.overrideAttrs (old: {
  shellHook = ''
    export PATH=$PATH:${python3Packages.python-language-server}/bin
  '';
  buildInputs = old.buildInputs ++ [ python3Packages.mypy ];
})
