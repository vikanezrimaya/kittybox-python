{ stdenv, fetchPypi, buildPythonPackage
, pytest, starlette, prometheus_client }:
buildPythonPackage rec {
  pname = "starlette-prometheus";
  version = "0.5.0";
  src = fetchPypi {
    inherit pname version;
    sha256 = "0ciydpj4ivbks5237qihj5w12z92pxcxk07w1xw538i5lv79y5a5";
  };
  propagatedBuildInputs = [
    starlette prometheus_client
  ];
  checkInputs = [ pytest ];
  checkPhase = ''
    pytest
  '';
  doCheck = false;
  #buildPhase = ''
  #  python ./make install
  #'';
  #checkPhase = ''
  #  python ./make test
  #'';
}
