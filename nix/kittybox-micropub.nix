{ stdenv, python3Packages
, withRedis ? true, redis
, withMongo ? false, mongodb }:
python3Packages.buildPythonPackage rec {
  pname = "kittybox-micropub";
  version = "0.1.0";
  src = ../.;

  propagatedBuildInputs = with python3Packages; [
    starlette starlette-prometheus
    mf2py dateutil markdown aiohttp ujson
    cchardet aiodns aiofiles multipart
    py-memoize
    hypercorn
  ]
  ++ (stdenv.lib.optionals withRedis [ python3Packages.aioredis python3Packages.hiredis ])
  ++ (stdenv.lib.optionals withMongo [ python3Packages.motor ]);
  checkInputs = with python3Packages; [
    aioresponses coverage pytest pytest-asyncio pytestcov
  ];

  checkPhase = ''
    ${stdenv.lib.optionalString withRedis ''
      # Launch Redis and Mongo in background
      # This works well within a sandbox
      # If you don't have sandbox enabled, that's your problem, not mine!
      #      ###
      #  #      #
      #      ##
      #  #      #
      #      ###
      ${redis}/bin/redis-server --save "" &
      export REDIS_URI="redis://localhost:6379"
    ''}
    pytest
    ${stdenv.lib.optionalString withRedis ''
      # Kill the Redis instance so it won't linger
      kill %1
    ''}
  '';

  #shellHook = ''
  #  PATH=$(nix-shell -p python3Packages.python-language-server --run 'echo $buildInputs')/bin:$PATH
  #'';

  meta = with stdenv.lib; {
    homepage = "https://git.fireburn.ru/projects/kittybox-micropub-starlette";
    description = "A central server for an IndieWeb-enabled blog - asyncio version which should be a lot faster";
    license = licenses.mit;
  };
}
