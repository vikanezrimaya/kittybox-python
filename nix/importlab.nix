{ stdenv, python3Packages, fetchPypi }:
python3Packages.buildPythonApplication rec {
  pname = "importlab";
  version = "0.5.1";
  src = fetchPypi {
    inherit pname version;
    sha256 = "07554r6njwvad2zjys8z0wvakwcg895nzznjmdxa246w346kamfq";
  };
  propagatedBuildInputs = with python3Packages; [ networkx six ];
}
