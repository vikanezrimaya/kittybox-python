{ stdenv, buildPythonPackage, fetchPypi, requests, html5lib, nose, mock, lxml, beautifulsoup4 }:
buildPythonPackage rec {
  pname = "mf2py";
  version = "1.1.2";
  src = fetchPypi {
    inherit pname version;
    sha256 = "1jg2xmdi1q09gykx8bv6dacm4m40rmy7wjdy60ffn79zzzrgiwc4";
  };
  propagatedBuildInputs = [
    requests
    html5lib
    nose
    mock
    lxml
    beautifulsoup4
  ];
  doCheck = false; # Test suite seems to require some files
  meta = with stdenv.lib; {
    description = "Microformats2 parser for Python";
  };
}
