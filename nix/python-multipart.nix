{ stdenv, python3Packages, fetchPypi }:
python3Packages.buildPythonPackage rec {
  pname = "python-multipart";
  version = "0.0.5";
  src = fetchPypi {
    inherit pname version;
    sha256 = "0hzshd665rl1bkwvaj9va4j3gs8nmb478fbvligx20663xhmzfzp";
  };
  propagatedBuildInputs = with python3Packages; [ six ];
  checkInputs = with python3Packages; [ pytest pytestcov pyyaml mock ];
  checkPhase = "py.test";
}
