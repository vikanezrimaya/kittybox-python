{ stdenv, python3Packages, fetchPypi }:
python3Packages.buildPythonPackage rec {
  pname = "py-memoize";
  version = "1.0.1";
  src = fetchPypi {
    inherit pname version;
    sha256 = "0kdyc7ckjlwa3ndifmipkxz2cdfrlhqqv2yig0bykz1fwly1vbaa";
  };
  doCheck = false;
}
