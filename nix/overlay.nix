let
  pythonOverlay = pkgs: pyself: pysuper: {
    mf2py = pyself.callPackage ./mf2py.nix {};
    mf2util = pyself.callPackage ./mf2util.nix {};
    fakeredis = pyself.callPackage ./fakeredis.nix {};
    kittybox-micropub = pyself.callPackage ./kittybox-micropub.nix {
      inherit (pkgs) redis mongodb;
    };
    py-memoize = pyself.callPackage ./py-memoize.nix {};
    hypercorn = pyself.callPackage ./hypercorn.nix {};
    multipart = pyself.callPackage ./python-multipart.nix {};
    pytype = pyself.callPackage ./pytype.nix {};
    importlab = pyself.callPackage ./importlab.nix {};
    starlette = pysuper.starlette.overrideAttrs (old: rec {
      name = "starlette-${version}";
      version = "0.13.0";
      src = pyself.fetchPypi {
        pname = "starlette";
        inherit version;
        sha256 = "1vxc0dil2kq3hgxnj3va6rl0id0qjzc0x93apz601l205lai9m3b";
      };
    });
    starlette-prometheus = pyself.callPackage ./starlette-prometheus.nix {};
  };
  overridePython = py: self: py.override { packageOverrides = pythonOverlay self; };
in
self: super: {
  python3 = overridePython super.python3 self;
  python37 = overridePython super.python37 self;
  python38 = overridePython super.python38 self;
  kittybox-micropub = super.python3Packages.toPythonApplication self.python3Packages.kittybox-micropub;
  hypercorn = super.python3Packages.toPythonApplication self.python3Packages.hypercorn;
  hypercorn-full = super.python3Packages.toPythonApplication self.python3Packages.hypercorn.override {
    enableUvloop = true;
    enableQuic = true;
    enableTrio = true;
  };
}
