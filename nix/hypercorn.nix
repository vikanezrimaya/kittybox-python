{ stdenv, python3Packages, fetchFromGitLab
, enableTrio ? false
, enableQuic ? false
, enableUvloop ? false }:

python3Packages.buildPythonPackage rec {
  pname = "hypercorn";
  version = "0.9.0";
  src = fetchFromGitLab {
    owner = "pgjones";
    repo = pname;
    rev = version;
    sha256 = "0zyvnlqv2j1qyrrr6rh8rfgnm1g2m4phcw5n6ylfhlil7880s3vc";
  };

  propagatedBuildInputs = with python3Packages; [ h11 h2 priority toml typing-extensions wsproto ]
  ++ (stdenv.lib.optionals enableTrio [ python3Packages.trio ])
  ++ (stdenv.lib.optionals enableQuic [ python3Packages.aioquic-prerelease ])
  ++ (stdenv.lib.optionals enableUvloop [ python3Packages.uvloop ]);

  # Not all dependencies are packaged.
  #testInputs = [ asynctest hypothesis pytest pytest-asyncio pytest-cov pytest-trio trio ];
  doCheck = false;
}
