{ stdenv, python3Packages, fetchPypi, ninja }:
python3Packages.buildPythonApplication rec {
  pname = "pytype";
  version = "2019.10.17";
  src = fetchPypi {
    inherit pname version;
    sha256 = "0kpgnrs11jvpwh2z42k9dgwx1fylnlc2k9ayi56xyk8lph5dshzz";
  };
  propagatedBuildInputs = with python3Packages; [ attrs importlab pyyaml six ninja typed-ast ];
}
